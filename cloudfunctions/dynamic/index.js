// 云函数入口文件
const dynamicList = require("./dynamicList/index")
const addDynamic = require("./addDynamic/index")
const dynamicDetail = require("./dynamicDetail")
const dynamicComment = require("./dynamicComment")
const dynamicEndorse = require("./dynamicEndorse")
const userDynamicList = require("./userDynamicList")
const dynamicTag = require("./dynamicTag/index")

const cloud = require('wx-server-sdk')
cloud.init()
// 云函数入口函数
exports.main = async (event, context) => {
  // const wxContext = cloud.getWXContext()
  const api = event.api //接口类型
  const params = event.data //所传递的数据
  let exportData = {}
  //错误码
  const responseCode = await cloud.callFunction({
    name: 'code'
  })
  if (api === "dynamic_list") {
    //获取动态列表
    const dynamicListResult = await dynamicList(params)
    exportData = Object.assign({
      data: dynamicListResult
    }, responseCode.result['00000'])
  } else if (api === "add_dynamic") {
    const addDynamicResult = await addDynamic(params)
    exportData = Object.assign({
      data: addDynamicResult
    }, responseCode.result['00000'])
  } else if (api === "dynamic_detail") {
    const dynamicDetailResult = await dynamicDetail(params)
    exportData = Object.assign({
      data: dynamicDetailResult
    }, responseCode.result['00000'])
  } else if (api === "dynamic_comment") {
    const dynamicCommentResult = await dynamicComment(params)
    exportData = Object.assign({
      data: dynamicCommentResult
    }, responseCode.result['00000'])
  } else if (api === "dynamic_endorse") {
    const dynamicEndorseResult = await dynamicEndorse(params)
    exportData = Object.assign({
      data: dynamicEndorseResult
    }, responseCode.result['00000'])
  } else if (api === "dynamic_user") {
    //用户的动态列表
    const dynamicUserResult = await userDynamicList(params)
    exportData = Object.assign({
      data: dynamicUserResult
    }, responseCode.result['00000'])
  } else if (api === 'dynamicTag') {
    const dynamicTagResult = await dynamicTag(params)
    exportData = Object.assign({
      data: dynamicTagResult
    }, responseCode.result['00000'])
  }

  return exportData
}