# 0基础部署教程

> 如果你只是一个新手，还不懂云开发。被我们团队的项目吸引到了，想下载下来测试测试，却可以不知道如何部署，本篇文章为你解答。
>
> 以下的操作步骤环境：Win10



## 注册小程序

想要使用云开发，你就必须要**注册**小程序了，不可以使用**测试号**了。

[注册小程序](https://jingyan.baidu.com/article/3ea5148984111c52e71bba7f.html)    不过多赘述这个了，不知道的可以搜索。使用邮箱注册的。



## 下载代码

找个好的文件夹，用git命令下载下来吧

> git clone https://gitee.com/huixi_and_their_friends/weiju-wechat-cloud.git



## 开通云开发

![](./readme-assets/image-20200907155934467.png)

![](./readme-assets/Snipaste_2020-09-07_16-04-51.png)

按照提示，一步一步来，有免费的额度的，不用怕。



## 建立数据库

![](./readme-assets/Snipaste_2020-09-07_16-07-45.png)

![](./readme-assets/Snipaste_2020-09-07_16-08-17.png)

![](./readme-assets/Snipaste_2020-09-07_16-16-47.png)

- appeal	诉求

  - 权限 所有用户可读，仅创建者可读写
  - 索引  coordinates![](./readme-assets/Snipaste_2020-09-10_11-32-19.png)

- appealComment    诉求评论

  - 权限 所有用户可读，仅创建者可读写

- appealEndorse    诉求点赞

  - 权限 所有用户可读，仅创建者可读写

- chat    聊天室

  - 权限 所有用户可读，仅创建者可读写

- chatRecord    聊天记录

  - 自定义权限：  

    ```
    {
      "read": "auth.openid in get(`database.chat.${doc.chatId}`).userIds",
      "write": "doc._openid == auth.openid"
    }
    ```

    

- dynamic    动态

  - 权限 所有用户可读，仅创建者可读写

- dynamicComment    动态评论

  - 权限 所有用户可读，仅创建者可读写

- dynamicEndorse    动态点赞

  - 权限 所有用户可读，仅创建者可读写

- user    用户

  - 权限 所有用户可读，仅创建者可读写



## 上传云函数

![](./readme-assets/Snipaste_2020-09-07_16-20-25.png)

![](./readme-assets/Snipaste_2020-09-07_16-20-52.png)

> 输入 npm install  下载依赖 （如果报错，那么你没有安装 node。 https://nodejs.org/en/   下载，安装就行）

![](./readme-assets/Snipaste_2020-09-07_16-23-46.png)

> cloudfunctions 文件夹下面的子文件夹都要这样哦

![](./readme-assets/Snipaste_2020-09-07_16-24-03.png)

> 在云开发 云函数 列表可以看到就代表成功了



## 愉快的测试

等不及了吧，赶快测试吧。有任何问题，可以去  [码云](https://gitee.com/huixi_and_their_friends/weiju-wechat-cloud) 提意见